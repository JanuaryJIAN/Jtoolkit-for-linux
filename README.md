`abspath`根据相对路径获取绝对路径

`stamp`给文本文件头部加上解释器，时间作者等等信息

`mv2app`把文件移动到系统应用图标存放的位置

`new_journal`生成一个以当前日期命名的markdown文件

`get_entry`用于建立一个`.desktop`文件

`setup.sh`用于在重装系统后完成自动配置

`backup2oss.py`用于打包一个目录并上传到阿里云oss